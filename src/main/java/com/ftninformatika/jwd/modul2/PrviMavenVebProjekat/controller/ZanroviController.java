package com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.model.Korisnik;
import com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.model.Zanr;
import com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.service.ZanrService;

@Controller
@RequestMapping(value="/Zanrovi")
public class ZanroviController {

	@Autowired
	private ZanrService zanrService;

	@GetMapping
	public String index(@RequestParam(required=false, defaultValue="") String naziv, 
			HttpSession session, ModelMap model) throws IOException {
		List<Zanr> zanrovi = zanrService.find(naziv);

		model.addAttribute("zanrovi", zanrovi);

		return "zanrovi";
	}

	@GetMapping(value="/Details")
	public String details(@RequestParam Long id, 
			HttpSession session, ModelMap model) throws IOException {
		Zanr zanr = zanrService.findOne(id);
		
		model.addAttribute("zanr", zanr);

		return "zanr";
	}

	@GetMapping(value="/Create")
	public String create(HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			return "redirect:/Zanrovi";
		}
		
		return "dodavanjeZanra";
	}

	@PostMapping(value="/Create")
	public String create(@RequestParam String naziv, 
			HttpSession session) throws IOException {
		// autentikacija, autorizacija
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik == null || !korisnik.isAdministrator()) {
			return "redirect:/Zanrovi";
		}

		// validacija
		if (naziv.equals("")) {
			return "redirect:/Zanrovi/Create";
		}

		// kreiranje
		Zanr zanr = new Zanr(naziv);
		zanrService.save(zanr);

		return "redirect:/Zanrovi";
	}

	@PostMapping(value="/Edit")
	public String edit(@RequestParam Long id, 
			@RequestParam String naziv, 
			HttpSession session) throws IOException {
		// autentikacija, autorizacija
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik == null || !korisnik.isAdministrator()) {
			return "redirect:/Zanrovi";
		}

		// validacija
		Zanr zanr = zanrService.findOne(id);
		if (zanr == null) {
			return "redirect:/Zanrovi";
		}	
		if (naziv == null || naziv.equals("")) {
			return "redirect:/Zanrovi/Details?id=" + id;
		}

		// izmena
		zanr.setNaziv(naziv);
		zanrService.update(zanr);

		return "redirect:/Zanrovi";
	}

	@PostMapping(value="/Delete")
	public String delete(@RequestParam Long id, 
			HttpSession session) throws IOException {
		// autentikacija, autorizacija
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik == null || !korisnik.isAdministrator()) {
			return "redirect:/Zanrovi";
		}

		// brisanje
		zanrService.delete(id);

		return "redirect:/Zanrovi";
	}

}
