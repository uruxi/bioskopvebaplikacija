package com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;

import com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.model.Film;
import com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.model.FilmStatistika;
import com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.model.Korisnik;
import com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.model.Zanr;
import com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.service.FilmService;
import com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.service.ZanrService;

@Controller
@RequestMapping(value="/Filmovi")
public class FilmoviController implements ServletContextAware {

	public static final String STATISTIKA_FILMOVA_KEY = "statistikaFilmova";
	public static final String POSECENI_FILMOVI_ZA_KORISNIKA_KEY = "poseceniFilmoviZaKorisnika";
	
	@Autowired
	private FilmService filmService;

	@Autowired
	private ZanrService zanrService;
	
	@Autowired
	private ServletContext servletContext;

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	// nema @ResponseBody
	@GetMapping
	public String index(
			@RequestParam(required=false, defaultValue="") String naziv, 
			@RequestParam(required=false, defaultValue="0") Long zanrId, 
			@RequestParam(required=false, defaultValue="0") Integer trajanjeOd, 
			@RequestParam(required=false, defaultValue="2147483647") Integer trajanjeDo, 
			HttpSession session, ModelMap model)  throws IOException {
		// čitanje
		// sledeće ne mora (a može) da se šalje template-u jer podacima iz sesije i iz context-a template može direktno da pristupa
		//Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

		List<Film> filmovi = filmService.find(naziv, zanrId, trajanjeOd, trajanjeDo);
		List<Zanr> zanrovi = zanrService.findAll();

		// popunjavanje modela
		model.addAttribute("filmovi", filmovi); // podatak koji se šalje template-u
		model.addAttribute("zanrovi", zanrovi); // podatak koji se šalje template-u
		// sledeće ne mora (a može) da se šalje template-u jer podacima iz sesije i iz context-a template može direktno da pristupa
		//model.addAttribute("prijavljeniKorisnik", prijavljeniKorisnik); // podatak koji se šalje template-u

		return "filmovi"; // prosleđivanje zahteva template-u
	}

	// nema @ResponseBody
	@GetMapping(value="/Details")
	@SuppressWarnings("unchecked")
	public String details(@RequestParam Long id, 
			HttpSession session, ModelMap model) throws IOException {
		// čitanje
		// sledeće ne mora (a može) da se šalje template-u jer podacima iz sesije i iz context-a template može direktno da pristupa
		//Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

		Film film = filmService.findOne(id);
		List<Zanr> zanrovi = zanrService.findAll();

		// obrada
		FilmStatistika statistikaFilmova = (FilmStatistika) servletContext.getAttribute(FilmoviController.STATISTIKA_FILMOVA_KEY);
		statistikaFilmova.incrementBrojac(film);

		List<Film> poseceniFilmovi = (List<Film>) session.getAttribute(FilmoviController.POSECENI_FILMOVI_ZA_KORISNIKA_KEY);
		if (!poseceniFilmovi.contains(film)) {
			poseceniFilmovi.add(film);
		}

		// popunjavanje modela
		model.addAttribute("film", film); // podatak koji se šalje template-u
		model.addAttribute("zanrovi", zanrovi); // podatak koji se šalje template-u
		// sledeće ne mora (a može) da se šalje template-u jer podacima iz sesije i iz context-a template može direktno da pristupa
		//model.addAttribute("prijavljeniKorisnik", prijavljeniKorisnik); // podatak koji se šalje template-u

		return "film"; // prosleđivanje zahteva template-u
	}

	// nema @ResponseBody
	@GetMapping(value="/Create")
	public String create(HttpSession session, ModelMap model) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			return "redirect:/Filmovi";
		}

		// čitanje
		List<Zanr> zanrovi = zanrService.findAll();

		// popunjavanje modela
		model.addAttribute("zanrovi", zanrovi); // podatak koji se šalje template-u
		// sledeće ne mora (a može) da se šalje template-u jer podacima iz sesije i iz context-a template može direktno da pristupa
		//model.addAttribute("prijavljeniKorisnik", prijavljeniKorisnik); // podatak koji se šalje template-u

		return "dodavanjeFilma"; // prosleđivanje zahteva template-u
	}

	@PostMapping(value="/Create")
	public String create(@RequestParam String naziv, @RequestParam int trajanje, @RequestParam(name="zanrId", required=false) Long[] zanrIds, 
			HttpSession session) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			return "redirect:/Filmovi";
		}

		// kreiranje
		Film film = new Film(naziv, trajanje);
		film.setZanrovi(zanrService.find(zanrIds));
		filmService.save(film);

		return "redirect:/Filmovi";
	}

	@PostMapping(value="/Edit")
	public String edit(@RequestParam Long id, 
			@RequestParam String naziv, @RequestParam int trajanje, @RequestParam(name="zanrId", required=false) Long[] zanrIds, 
			HttpSession session) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			return "redirect:/Filmovi";
		}

		// validacija
		Film film = filmService.findOne(id);
		if (film == null) {
			return "redirect:/Filmovi";
		}	
		if (naziv == null || naziv.equals("") || trajanje < 5) {
			return "redirect:/Filmovi/Details?id=" + id;
		}

		// izmena
		film.setNaziv(naziv);
		film.setTrajanje(trajanje);
		film.setZanrovi(zanrService.find(zanrIds));
		filmService.update(film);

		return "redirect:/Filmovi";
	}

	@PostMapping(value="/Delete")
	public String delete(@RequestParam Long id, 
			HttpSession session) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			return "redirect:/Filmovi";
		}

		// brisanje
		filmService.delete(id);
	
		return "redirect:/Filmovi";
	}

}
