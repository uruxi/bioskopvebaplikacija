package com.ftninformatika.jwd.modul2.PrviMavenVebProjekat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/")
public class IndexController {

	@GetMapping
	public String index() {
		// naziv template-a na koga se zahtev prosleđuje
		return "index";
	}

}
