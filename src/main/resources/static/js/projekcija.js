function validacija() {
	let forma = document.forms[0];
	let salaInput = forma.sala;

	let tipSelect = document.getElementsByName("tip")[0];

	let pasusGreska = document.getElementsByClassName("greska")[0];
	
	let sala = salaInput.value;
	let tip = "";
	for (let itOpcija of tipSelect.options) {
		if (itOpcija.selected) {
			tip = itOpcija.value;
			break;
		}
	}
	
	if (tip == "3D" && sala != "2") {
		pasusGreska.textContent = "Samo sala 2 može da prikazuje 3D snimke!";
		return false;
	}
	if (tip == "4D" && sala != "3") {
		pasusGreska.textContent = "Samo sala 3 može da prikazuje 4D snimke!";
		return false;
	}

	return true;
}