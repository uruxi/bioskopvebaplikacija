function registracija() {
	var forma = document.forms[0];
	var korisnickoImeInput = forma.korisnickoIme;
	var lozinkaInput = forma.lozinka;
	var ponovljenaLozinkaInput = forma.ponovljenaLozinka;
	var eMailInput = forma.eMail;
	
	var pasusGreska = document.getElementsByClassName("greska")[0];

	var korisnickoIme = korisnickoImeInput.value;
	var lozinka = lozinkaInput.value;
	var ponovljenaLozinka = ponovljenaLozinkaInput.value;
	var eMail = eMailInput.value;
	
	if (korisnickoIme == "" || lozinka == "" || ponovljenaLozinka == "" || eMail == "") {
		pasusGreska.innerHTML = "Niste popunili sva polja!";
		return false;
	}
	if (lozinka != ponovljenaLozinka) {
		pasusGreska.innerHTML = "Lozinke se ne poklapaju!";
		return false;
	}
	if (korisnickoIme.match("^[a-zA-Z0-9]+$") == null) {
		pasusGreska.innerHTML = "Korisničko ime sme da sadrži samo alfanumeričke karaktere!";
		return false;
	}
	if (eMail.match("^[a-zA-Z0-9]+@[a-zA-Z0-9]+.com$") == null) {
		pasusGreska.innerHTML = "E-mail nije u odgovarajućem obliku!";
		return false;
	}

	return true;
}