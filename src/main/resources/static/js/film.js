function validacija() {
	let forma = document.forms[0];
	let nazivInput = forma.naziv;
	let trajanjeInput = forma.trajanje;

	let zanrCheckboxovi = document.getElementsByName("zanrId");

	let pasusGreska = document.getElementsByClassName("greska")[0];

	let naziv = nazivInput.value;
	let trajanje = trajanjeInput.value;
	
	if (naziv == "" || trajanje == "") {
		pasusGreska.textContent = "Niste popunili sva polja!";
		return false;
	}
	let checkiranBarJedanZanr = false;
	for (let itCheckbox of zanrCheckboxovi) {
		if (itCheckbox.checked) {
			checkiranBarJedanZanr = true;
			break;
		}
	}
	if (!checkiranBarJedanZanr) {
		pasusGreska.textContent = "Morate odabrati bar jedan žanr!";
		return false;
	}
	if (trajanje.match("^[0-9]+$") == null || parseInt(trajanje) < 5) {
		pasusGreska.textContent = "Trajanje mora biti ceo broj veći od 5!";
		return false;
	}

	return true;
}

function minus(dugme) {
	let trajanjeInput = dugme.nextSibling;
	
	let trajanje = parseInt(trajanjeInput.value) - 1;
	if (trajanje < 5) {
		trajanje = 5;
	}
	trajanjeInput.value = trajanje;
	
	return false; // dugme koje je u formi takođe pokreće submit događaj
}

function plus(dugme) {
	let trajanjeInput = dugme.previousSibling;
	
	let trajanje = parseInt(trajanjeInput.value) + 1;
	trajanjeInput.value = trajanje;
	
	return false; // dugme koje je u formi takođe pokreće submit događaj
}