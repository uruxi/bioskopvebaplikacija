function sakrivanje(checkbox) {
	let red = document.getElementsByTagName("tr")[4]; // preskoči 3 reda u tabeli za korisnika i prvi red u tabeli za prikaz
	if (checkbox.checked) {
		red.style.display = "none";
	} else {
		red.style.display = "table-row";
	}
}

function farbanje(select) {
	let boja = "";
	for (let itOpcija of select.options) {
		if (itOpcija.selected) {
			boja = itOpcija.value;
			break;
		}
	}
	
	let sviTh = document.getElementsByTagName("th");
	for (let itTh of sviTh) {
		itTh.style.backgroundColor = boja;
	}
}