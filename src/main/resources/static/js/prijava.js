function prijava() {
	let forma = document.forms[0];
	let korisnickoImeInput = forma.korisnickoIme;
	let lozinkaInput = forma.lozinka;
	
	let pasusGreska = document.getElementsByClassName("greska")[0];

	let korisnickoIme = korisnickoImeInput.value;
	let lozinka = lozinkaInput.value;
	console.log("ki: " + korisnickoIme);
	console.log("l: " + lozinka);
	
	if (korisnickoIme == "" || lozinka == "") {
		//alert("Niste popunili sva polja!")
		pasusGreska.textContent = "Niste popunili sva polja!";
		return false;
	}

	return true;
}

function sakrivanje(checkbox) {
	let lozinkaInput = document.getElementsByName("lozinka")[0];
	if (checkbox.checked) {
		lozinkaInput.setAttribute("type", "password");
	} else {
		lozinkaInput.setAttribute("type", "text");
	}
}